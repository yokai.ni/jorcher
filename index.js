const request = require( "request" );
const sleep = require("sleep").sleep;
const Datastore = require('nedb')

var db                             =                 new Datastore({ filename: './db/posts.nedb', autoload: true }),
    allowed                        =                 require( "./filters/allowed.json" ),
    forbidden                      =                 require( "./filters/forbidden.json" ),
    query                          =                 encodeURIComponent( "#программист" ),
    now                            =                 function() { return Math.floor(Date.now() / 1000); };

var lastreq = now();

//this function will request all posts for query since lastreq (unixtime)
function tick() {
    var reqUrl = "https://api.vk.com/method/newsfeed.search?q=" + query
    + "&count=" + 200 + "&start_time=" + (lastreq + 1);
    //console.log(reqUrl); //for debug
    request( reqUrl, reqhandler );
}

//this function will be used for loading all posts from past that it can load
function search_since(last) {
    lastreq = last;
    tick();
}

//handler for request
var reqhandler = function( error, response, body ) {
    if (!error && response.statusCode == 200) {
    var result = JSON.parse(body).response;
        for (let i = 0; i < result.length; i++) {
            //if post have some text attached
            if (result[i].hasOwnProperty("text")) { 
                let isForbidden = false; let isAllowed = 0;
                for (let j=0; j<forbidden.length; j++) {
                    //mark it forbidden if it matches forbidden.json filters
                    if (result[i].text.match(new RegExp(forbidden[j], 'im')) != null) isForbidden = true; 
                }
                
                if (!isForbidden) {
                    for (let j=0; j<allowed.length; j++) {
                        //give a numeric result about how many filters from allowed.json matched the post
                        if (result[i].text.match(new RegExp(allowed[j], 'im')) != null) isAllowed++;
                    }
                    if (isAllowed >= 2) {
                        let toInsert = result[i];
                        toInsert.rating = isAllowed;
                        toInsert._id = "id" + result[i].id;
                        db.insert(toInsert);
                    } 
                }
            }
        }
    }
    //as we handling request, last request was completed now
    lastreq = now();
}

function preload_lastday() {
    var state_preload = 0;
    var inow = now();
    while (state_preload < (6)) {
        state_preload++;
        search_since(inow - (6 - state_preload) * 4 * 60 * 60);
        sleep(5);
    }
}

preload_lastday();
var mainInterval = setInterval(tick, 60000);
